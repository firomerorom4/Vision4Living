'use strict';
console.log("Starting Vision4Living " + new Date());
var deviceReadyFired = false;
var EVENT_DEVICEREADY = "deviceready";
var onDeviceReady = function()
{
    console.log("Firing 'deviceready'");
    // Set status bar to "default" dark text
    //angular.bootstrap(document, ['app']);
    deviceReadyFired = true;
};

if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/))
{
    document.addEventListener(EVENT_DEVICEREADY, onDeviceReady, false);
} else
{
    window.setTimeout(function() {
        var e = document.createEvent('Events');
        e.initEvent(EVENT_DEVICEREADY, true, false);
        document.dispatchEvent(e);
        onDeviceReady();
    }, 50);
}
angular.module('vision4living', ['onsen','ui.router','ngTouch'])
    .config(function ($stateProvider,$urlRouterProvider) {
        configureRouting($stateProvider, $urlRouterProvider);
    })
    
    ;