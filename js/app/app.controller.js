'use strict';
angular.module('vision4living').controller('VisionController',function ($scope,$timeout) {
    var documents  = [];

    for (var i = 0; i < 100; i++) 
    {
        documents.push({
            "title":"My title" +i*2,
            "content":"Lorem ipsum"+i*3,
            "note":"Anima"+i*4
        });
    }

    $scope.documents = documents;
    $scope.current = {
        "title":'',
        "content":'',
        "note":''
    };

    $scope.setCurrent = function (doc)
    {
        if ($scope.current.title=='')
        {
           $timeout(function(){
               $scope.current = angular.copy(doc);
           },200)
        }
        else
        {

           $timeout(function(){
               $scope.current =
               {
                   "title":'',
                   "content":'',
                   "note":''
               };
           },200);

        }
    }

    

});