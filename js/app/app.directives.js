'use strict';

/**
 * Swipe Left Action
 */
angular.module('vision4living').directive('swipeleft', function() {
    return function(scope, element, attrs) {
        element.bind("swipeleft", function(event) {
            scope.$apply(function() {
                scope.$eval(attrs.swipeleft);
            });

            event.preventDefault();
        });
    };
});

/**
 * Swipe Right Action
 */
angular.module('vision4living').directive('swiperight', function() {
    return function(scope, element, attrs) {
        element.bind("swiperight", function(event) {
            scope.$apply(function() {
                scope.$eval(attrs.swiperight);
            });

            event.preventDefault();
        });
    };
});
